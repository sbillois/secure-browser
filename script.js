const userAgent = navigator.userAgent.toLowerCase();

if (userAgent.indexOf('firefox') !== -1) {
    // Firefox
    
} else if (userAgent.indexOf('opera') !== -1) {
  if (userAgent.indexOf('opr') !== -1) {
      // Opera (basé sur Chromium)
      
  } else {
      // Opera (non basé sur Chromium)
      
  }
} else if (userAgent.indexOf('edge') !== -1) {
  if (userAgent.indexOf('edg') !== -1) {
      // Edge (basé sur Chromium)
      
  } else {
      // Edge (non basé sur Chromium)
      
  }
} else if (userAgent.indexOf('vivaldi') !== -1) {
    // Vivaldi
    
} else if (userAgent.indexOf('safari') !== -1) {
    // Safari
    
} else if (userAgent.indexOf('msie') !== -1 || userAgent.indexOf('trident') !== -1) {
    // MSIE
    
} else if (userAgent.indexOf('chrome') !== -1) {
  if (userAgent.indexOf('chromium') !== -1) {
      // Chromium
      
  } else if (userAgent.indexOf('brave') !== -1) {
      // Brave
      
  } else if (userAgent.indexOf('maxthon') !== -1) {
      // Maxthon
      
  } else if (userAgent.indexOf('torch') !== -1) {
      // Torch
      
  } else if (userAgent.indexOf('avant') !== -1) {
      // Avant Browser
      
  } else if (userAgent.indexOf('palemoon') !== -1) {
      // Pale Moon
      
  } else if (userAgent.indexOf('waterfox') !== -1) {
      // Waterfox
      
  } else if (userAgent.indexOf('seamonkey') !== -1) {
      // SeaMonkey
      
  } else if (userAgent.indexOf('slimjet') !== -1) {
      // Slimjet
      
  } else if (userAgent.indexOf('iridium') !== -1) {
      // Iridium
      
  } else if (userAgent.indexOf('qutebrowser') !== -1) {
      // Qutebrowser
      
  } else if (userAgent.indexOf('otter') !== -1) {
      // Otter Browser
      
  } else if (userAgent.indexOf('konqueror') !== -1) {
      // Konqueror
      
  } else if (userAgent.indexOf('falkon') !== -1) {
      // Falkon
      
  } else if (userAgent.indexOf('epiphany') !== -1 || userAgent.indexOf('gnome-web') !== -1) {
      // GNOME Web (Epiphany)
      
  } else if (userAgent.indexOf('icecat') !== -1) {
      // IceCat
      
  } else if (userAgent.indexOf('kameleon') !== -1) {
      // Kameleon
      
  } else {
      // Google Chrome (ou autre navigateur non reconnu)
      redAlert("Attention : Vous utilisez un navigateur non-libre ou non-opensource")
  }
} else if (userAgent.indexOf('lynx') !== -1) {
    // Lynx
    
} else if (userAgent.indexOf('netscape') !== -1) {
    // Netscape Navigator
    
} else {
    // Autre navigateur
    
}

const redAlert = (mes) => {
  const body = document.querySelector('body');
  const warningBanner = document.createElement('div');
  warningBanner.textContent = mes;
  warningBanner.style.backgroundColor = 'red';
  warningBanner.style.color = 'white';
  warningBanner.style.padding = '10px';
  warningBanner.style.textAlign = 'center';
  warningBanner.style.fontWeight = 'bold';
  warningBanner.style.position = 'fixed';
  warningBanner.style.top = '0';
  warningBanner.style.left = '0';
  warningBanner.style.width = '100%';
  body.insertBefore(warningBanner, body.firstChild);


}
